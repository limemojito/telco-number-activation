package integration;

import com.limemojito.telco.phone.activate.model.PhoneNumber;
import integration.driver.ApplicationApi;
import integration.driver.PhoneNumberApi;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.reactive.function.client.WebClientResponseException.BadRequest;
import org.springframework.web.reactive.function.client.WebClientResponseException.NotFound;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * These are some example integration tests for the customer endpoint.  Note how we get validation responses, pagination, etc from HATEOAS out
 * of the box or with minimal annotations.     The pattern here is for integration tests to check the configuration of the spring data application
 * including some error handling.  Note that this should NOT be for all validation scenarios, rather to check that wiring and error protocols
 * function as expected.  Design pattern is taking a driver and ApiModel approach, in a similar vein to how you would do a PageModel approach in
 * Cucumber or Selenium style integration or functional testing.
 */
public class PhoneNumberIT {

    private PhoneNumberApi phoneNumberApi;

    @Before
    public void setup() throws URISyntaxException {
        ApplicationApi applicationApi = new ApplicationApi("http://localhost:8080/");
        applicationApi.clearData();

        phoneNumberApi = applicationApi.getPhoneNumberApi();
    }

    @Test
    public void shouldCreatePhoneNumberOk() {
        assertThat(phoneNumberApi.getCount()).isEqualTo(0);

        final UUID customerId = UUID.randomUUID();
        final PhoneNumber phoneNumber = PhoneNumber.builder()
                                                   .number("414646441")
                                                   .customerId(customerId)
                                                   .build();
        phoneNumberApi.create(phoneNumber);

        assertThat(phoneNumberApi.getCount()).isEqualTo(1);

        assertThat(phoneNumberApi.get("414646441").isActive()).isFalse();
    }

    @Test(expected = BadRequest.class)
    public void shouldFailSendingBadNumber() {
        final PhoneNumber phoneNumber = PhoneNumber.builder()
                                                   .number("0414646411")
                                                   .customerId(UUID.randomUUID())
                                                   .build();
        phoneNumberApi.create(phoneNumber);
    }

    @Test
    public void shouldGetPhoneNumbersOfSingleCustomer() {
        phoneNumberApi.create(PhoneNumber.builder()
                                         .number("976584493")
                                         .customerId(UUID.randomUUID())
                                         .build());
        final UUID customerId = UUID.randomUUID();
        phoneNumberApi.create(PhoneNumber.builder()
                                         .number("414646441")
                                         .customerId(customerId)
                                         .build());

        List<PhoneNumber> numbers = phoneNumberApi.findByCustomerId(customerId);
        assertThat(numbers).hasSize(1);
    }

    @Test
    public void shouldActivateAndAddAnActivation() {
        final UUID customerId = UUID.randomUUID();
        final String number = "414646441";
        phoneNumberApi.create(PhoneNumber.builder()
                                         .number(number)
                                         .customerId(customerId)
                                         .build());
        phoneNumberApi.activate(number);

        assertThat(phoneNumberApi.get("414646441").isActive()).isTrue();
    }

    @Test
    public void shouldSendAFailActivateThatAppearsInHistory() {
        final UUID customerId = UUID.randomUUID();
        final String number = "414646441";
        phoneNumberApi.create(PhoneNumber.builder()
                                         .number(number)
                                         .customerId(customerId)
                                         .build());
        phoneNumberApi.activate(number, false);

        // to make sure the timestamps are apart
        assertThat(phoneNumberApi.get("414646441").isActive()).isFalse();

        phoneNumberApi.activate(number, true);
        assertThat(phoneNumberApi.get("414646441").isActive()).isTrue();
    }

    @Test
    public void shouldHaveMultipleValuesInHistory() throws Exception {
        final UUID customerId = UUID.randomUUID();
        final String number = "414646441";
        phoneNumberApi.create(PhoneNumber.builder()
                                         .number(number)
                                         .customerId(customerId)
                                         .build());
        phoneNumberApi.activate(number, false);
        phoneNumberApi.activate(number, false);
        phoneNumberApi.activate(number, true);

        // to make sure the timestamps are apart
        final PhoneNumber phoneNumber = phoneNumberApi.get("414646441");
        assertThat(phoneNumber.isActive()).isTrue();
        assertThat(phoneNumber.getActivationHistory()).hasSize(3);
        assertThat(phoneNumber.getActivationHistory().get(0).getSuccess()).isTrue();
    }

    @Test(expected = NotFound.class)
    public void shouldFailOnMissingNumber() {
        phoneNumberApi.activate("6547328112");
    }
}
