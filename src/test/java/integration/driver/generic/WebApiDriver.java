package integration.driver.generic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.client.Traverson;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;

@Slf4j
public class WebApiDriver {
    private final Traverson traverson;
    private final WebClient webClientThatMakesSeparateWebRequestsToTraverson;

    public WebApiDriver(Traverson traverson, WebClient webClient) {
        this.traverson = traverson;
        this.webClientThatMakesSeparateWebRequestsToTraverson = webClient;
    }

    public void delete(String uri) {
        final ClientResponse spec = webClientThatMakesSeparateWebRequestsToTraverson.delete().uri(uri).exchange().block();
        assert spec != null;
        if (spec.statusCode().isError()) {
            log.warn("Error deleting {}: {}", uri, spec.statusCode().toString());
        }
    }

    public <CallType> List<Link> collectListReferences(String relativeUrl,
                                                       String objectRef,
                                                       ParameterizedTypeReference<PagedResources<Resource<CallType>>> listType) {
        final List<Link> collector = new LinkedList<>();
        for (Resource<?> resource : traverson.follow(relativeUrl).toObject(listType)) {
            collector.add(resource.getLink(objectRef));
        }
        return collector;
    }

    public <CallType> void followForEach(String relativeUrl,
                                         ParameterizedTypeReference<PagedResources<Resource<CallType>>> listType,
                                         Consumer<Resource<CallType>> action) {
        traverson.follow(relativeUrl).toObject(listType).forEach(action);
    }

    public <CallType> void followForEachResource(String relativeUrl,
                                                 ParameterizedTypeReference<PagedResources<Resource<CallType>>> listType,
                                                 Consumer<CallType> action) {
        traverson.follow(relativeUrl).toObject(listType).forEach((o) -> action.accept(o.getContent()));
    }


    public <CallType, RequestType> CallType put(Class<CallType> responseType, String uri, RequestType body, Object... uriParameters) {
        return bind(responseType, uri, body, webClientThatMakesSeparateWebRequestsToTraverson.put(), uriParameters);
    }

    public <CallType, RequestType> CallType post(Class<CallType> responseType, String uri, RequestType body, Object... uriParameters) {
        return bind(responseType, uri, body, webClientThatMakesSeparateWebRequestsToTraverson.post(), uriParameters);
    }

    public <CallType, RequestType> CallType patch(Class<CallType> responseType, String href, RequestType body, Object... uriParameters) {
        return bind(responseType, href, body, webClientThatMakesSeparateWebRequestsToTraverson.patch(), uriParameters);
    }

    public <CallType> CallType get(Class<CallType> responseType, String uri, Object... uriParameters) {
        return webClientThatMakesSeparateWebRequestsToTraverson.get()
                                                               .uri(uri, uriParameters)
                                                               .retrieve()
                                                               .bodyToMono(responseType)
                                                               .block();
    }

    public <CallType> CallType get(ParameterizedTypeReference<CallType> responseType, String uri, Object... uriParameters) {
        return webClientThatMakesSeparateWebRequestsToTraverson.get()
                                                               .uri(uri, uriParameters)
                                                               .retrieve()
                                                               .bodyToMono(responseType)
                                                               .block();
    }

    public Integer getListSize(String relativeUrl) {
        return traverson.follow(relativeUrl).toObject("$.page.totalElements");
    }

    private <CallType, RequestType> CallType bind(Class<CallType> responseType,
                                                  String uri,
                                                  RequestType body,
                                                  WebClient.RequestBodyUriSpec uriSpec,
                                                  Object[] uriParameters) {
        final WebClient.RequestBodySpec spec = uriSpec.uri(uri, uriParameters);
        if (body != null) {
            spec.body(fromObject(body));
        }
        return spec.retrieve()
                   .bodyToMono(responseType)
                   .block();
    }

    public Traverson getTraverson() {
        return traverson;
    }
}
