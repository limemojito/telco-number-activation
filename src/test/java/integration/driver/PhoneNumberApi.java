package integration.driver;

import com.limemojito.telco.phone.activate.model.PhoneNumber;
import integration.driver.generic.WebApiDriver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class PhoneNumberApi {
    private final WebApiDriver apiDriver;
    private final String relativeUrl = "phoneNumbers";
    private ParameterizedTypeReference<PagedResources<Resource<PhoneNumber>>> listType = new ParameterizedTypeReference<>() {
    };

    public PhoneNumberApi(WebApiDriver apiDriver) {
        this.apiDriver = apiDriver;
    }

    public PhoneNumber create(PhoneNumber phoneNumber) {
        return apiDriver.post(PhoneNumber.class, relativeUrl, phoneNumber);
    }

    public void deleteAll() {
        apiDriver.followForEach(relativeUrl, listType, (object) -> apiDriver.delete(object.getId().getHref()));
    }

    public int getCount() {
        return apiDriver.getListSize(relativeUrl);
    }

    public List<PhoneNumber> findByCustomerId(UUID customerId) {
        final Map<String, Object> uuidMap = Map.of("customerId", customerId);
        final List<PhoneNumber> results = new LinkedList<>();
        final PagedResources<Resource<PhoneNumber>> found = apiDriver.getTraverson()
                                                                     .follow(relativeUrl)
                                                                     .follow("search")
                                                                     .follow("findByCustomerId")
                                                                     .withTemplateParameters(uuidMap)
                                                                     .toObject(listType);
        for (Resource<PhoneNumber> resource : found) {
            results.add(resource.getContent());
        }
        return results;
    }

    public PhoneNumber get(String number) {
        return apiDriver.get(PhoneNumber.class, relativeUrl + "/{number}", number);
    }

    public void activate(String number) {
        apiDriver.post(PhoneNumber.class, relativeUrl + "/{number}/activate", Void.class, number);
    }

    public void activate(String number, boolean state) {
        apiDriver.post(PhoneNumber.class, relativeUrl + "/{number}/activate?success={success}", Void.class, number, state);
    }
}
