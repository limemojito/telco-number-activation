package integration.driver;

import integration.driver.generic.WebApiDriver;
import lombok.Getter;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URI;
import java.net.URISyntaxException;

@Getter
public class ApplicationApi {
    private final PhoneNumberApi phoneNumberApi;

    public ApplicationApi(String baseUrl) throws URISyntaxException {
        final Traverson traverson = new Traverson(new URI(baseUrl), MediaTypes.HAL_JSON);
        final WebClient webClient = WebClient.create(baseUrl);
        final WebApiDriver apiDriver = new WebApiDriver(traverson, webClient);
        phoneNumberApi = new PhoneNumberApi(apiDriver);
    }

    public void clearData() {
        phoneNumberApi.deleteAll();
    }
}
