package unit;

import com.limemojito.telco.phone.activate.ActivationController;
import com.limemojito.telco.phone.activate.model.PhoneNumber;
import com.limemojito.telco.phone.activate.model.PhoneNumberRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.*;

public class ActivationControllerTest {
    @Rule
    public MethodRule mockito = MockitoJUnit.rule();

    @Mock
    private PhoneNumberRepository phoneRepo;

    @Captor
    private ArgumentCaptor<PhoneNumber> persistCaptor;

    private ActivationController controller;

    @Before
    public void controller() {
        controller = new ActivationController(phoneRepo);
    }

    @Test(expected = ActivationController.PhoneNumberNotFound.class)
    public void shouldFailIfPhoneNumberNotFound() throws Exception {
        final String phoneNumber = "565564744";
        when(phoneRepo.findById(phoneNumber)).thenReturn(Optional.empty());
        controller.activate(phoneNumber, true);
    }

    @Test
    public void shouldUpdatePhoneNumberActivationHistory() throws Exception {
        final String phoneNumber = "565564744";
        final PhoneNumber number = PhoneNumber.builder().number(phoneNumber).build();
        assertThat(number.getActivationHistory()).isEmpty();

        when(phoneRepo.findById(phoneNumber)).thenReturn(Optional.of(number));
        when(phoneRepo.save(persistCaptor.capture())).thenAnswer(a -> a.getArgument(0));

        controller.activate(phoneNumber, true);

        verify(phoneRepo).save(number);
        PhoneNumber persisted = persistCaptor.getValue();
        assertThat(persisted.getActivationHistory()).isNotEmpty();
        assertThat(persisted.isActive()).isTrue();
    }

    @Test
    public void shouldUpdatePhoneNumberActivationHistoryOnFalse() throws Exception {
        final String phoneNumber = "565564744";
        final PhoneNumber number = PhoneNumber.builder().number(phoneNumber).build();
        assertThat(number.getActivationHistory()).isEmpty();

        when(phoneRepo.findById(phoneNumber)).thenReturn(Optional.of(number));
        when(phoneRepo.save(persistCaptor.capture())).thenAnswer(a -> a.getArgument(0));

        controller.activate(phoneNumber, false);

        verify(phoneRepo).save(number);
        PhoneNumber persisted = persistCaptor.getValue();
        assertThat(persisted.getActivationHistory()).isNotEmpty();
        assertThat(persisted.isActive()).isFalse();
    }

    @Test
    public void shouldUpdatePhoneNumberActivationHistoryOnFalseAndTrue() throws Exception {
        final String phoneNumber = "565564744";
        final PhoneNumber number = PhoneNumber.builder().number(phoneNumber).build();
        assertThat(number.getActivationHistory()).isEmpty();

        when(phoneRepo.findById(phoneNumber)).thenReturn(Optional.of(number));
        when(phoneRepo.save(persistCaptor.capture())).thenAnswer(a -> a.getArgument(0)).thenAnswer(a -> a.getArgument(0));

        controller.activate(phoneNumber, false);
        assertThat(persistCaptor.getValue().isActive()).isFalse();

        controller.activate(phoneNumber, true);

        verify(phoneRepo, times(2)).save(number);
        assertThat(persistCaptor.getValue().getActivationHistory()).hasSize(2);
        assertThat(persistCaptor.getValue().isActive()).isTrue();
    }
}
