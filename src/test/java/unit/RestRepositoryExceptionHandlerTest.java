package unit;

import com.limemojito.telco.phone.activate.RestResponseEntityExceptionHandler;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import static java.util.Set.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * Spring data rest annoyingly buries the validation handler at the end of the event chain, resulting in valdation failures being inside a
 * transaction failure.
 */
public class RestRepositoryExceptionHandlerTest {
    private final RestResponseEntityExceptionHandler handler = new RestResponseEntityExceptionHandler();

    @Rule
    public MethodRule mockito = MockitoJUnit.rule();

    @Mock
    private ConstraintViolation<?> violationMock;

    @Test
    public void shouldHandleUnexpectedException() {
        final ResponseEntity<Object> entity = handler.handleTransactionFailure(new RuntimeException("BANG"));
        assertThat(entity.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldHandleUnexpectedTransactionException() {
        final ResponseEntity<Object> entity = handler.handleTransactionFailure(new TransactionSystemException("BANG"));
        assertThat(entity.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldHandleUnexpectedTransactionCauseException() {
        final ResponseEntity<Object> entity = handler.handleTransactionFailure(new TransactionSystemException("Surprise",
                                                                                                              new RuntimeException("BANG")));
        assertThat(entity.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldHandleUnexpectedRollbackCauseException() {
        final ResponseEntity<Object> entity = handler.handleTransactionFailure(new TransactionSystemException("RollbackException",
                                                                                                              new RollbackException(new RuntimeException(
                                                                                                                      "Bang"))));
        assertThat(entity.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldHandleConstraintOnRollbackExceptionCausingTransactionException() throws Exception {
        when(violationMock.getMessage()).thenReturn("This is a violation");
        final ConstraintViolationException constraintViolationException = new ConstraintViolationException("bang", of(violationMock));
        final RollbackException rollbackException = new RollbackException("Validation", constraintViolationException);
        final TransactionSystemException transactionSystemException = new TransactionSystemException("RollbackException", rollbackException);

        final ResponseEntity<Object> entity = handler.handleTransactionFailure(transactionSystemException);

        assertThat(entity.getStatusCode()).isEqualTo(BAD_REQUEST);
        assertThat(entity.getBody()).isEqualTo("This is a violation");
        verify(violationMock).getMessage();
    }
}
