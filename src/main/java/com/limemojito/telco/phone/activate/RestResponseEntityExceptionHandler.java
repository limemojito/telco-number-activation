package com.limemojito.telco.phone.activate;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * This is slightly hacky to work around the known issue with Rest Hateoas having the default validator at the end of the validation chain.
     *
     * @param ex Exception to handle.
     * @return A response entity with an appropriate error code.  Bad Request on a validation error, INTERNAL_SERVER_ERROR otherwise.
     * @see org.springframework.http.HttpStatus
     */
    @ExceptionHandler({TransactionSystemException.class})
    public ResponseEntity<Object> handleTransactionFailure(Exception ex) {
        if (ex.getCause() instanceof RollbackException && ex.getCause().getCause() instanceof ConstraintViolationException) {
            ConstraintViolationException nevEx = (ConstraintViolationException) ex.getCause().getCause();

            String errors = nevEx.getConstraintViolations().stream()
                                 .map(ConstraintViolation::getMessage).collect(Collectors.joining("\n"));

            return new ResponseEntity<>(errors, new HttpHeaders(), BAD_REQUEST);
        } else {
            return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), INTERNAL_SERVER_ERROR);
        }
    }
}
