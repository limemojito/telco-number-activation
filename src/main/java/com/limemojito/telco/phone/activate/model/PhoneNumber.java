package com.limemojito.telco.phone.activate.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;

@SuppressWarnings("MagicNumber")
@Entity
@Table(name = "phone_number")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@NamedEntityGraph(name = "allHistory", includeAllAttributes = true)
public class PhoneNumber {
    /**
     * Number using a regex check, must be length 9 and no leading 0 (state dial prefix).
     */
    @Id
    @NotNull
    @Pattern(regexp = "^([1-9][0-9]{0,8})$", message = "Failed phone number validation (leading zero present?)")
    @Column(name = "number")
    private String number;

    @NotNull
    @Column(name = "customer_id", columnDefinition = "uuid", nullable = false)
    private UUID customerId;

    @Transient
    public boolean isActive() {
        return activationHistory.isEmpty() ? false : activationHistory.get(0).getSuccess();
    }

    /**
     * Activation history is always ordered (when loaded from the DB) in descending activation attempt order (ie latest attempt first).
     */
    @Builder.Default
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "phoneNumber", cascade = ALL, orphanRemoval = true)
    @OrderBy("attemptTime DESC")
    private List<ActivationAttempt> activationHistory = new LinkedList<>();
}
