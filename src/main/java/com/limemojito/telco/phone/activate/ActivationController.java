package com.limemojito.telco.phone.activate;

import com.limemojito.telco.phone.activate.model.ActivationAttempt;
import com.limemojito.telco.phone.activate.model.PhoneNumber;
import com.limemojito.telco.phone.activate.model.PhoneNumberRepository;
import org.springframework.web.bind.annotation.*;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RequestMapping("/phoneNumbers/{number}/activate")
@RestController
public class ActivationController {

    private final PhoneNumberRepository phoneNumberRepository;

    public ActivationController(PhoneNumberRepository phoneNumberRepository) {
        this.phoneNumberRepository = phoneNumberRepository;
    }

    /**
     * Representing an async style api here - we return a success but no content as in reality activation would go off and call provisioning
     * systems, probably not in realtime.
     *
     * @param phoneNumber Phone Number to activate
     * @param success     A handy parameter for remote testing - defaults to true but you can add false as the first ActivationAttempt.
     * @throws PhoneNumberNotFound if the phone number can not be found
     * @see ActivationAttempt
     */
    @PostMapping
    @ResponseStatus(NO_CONTENT)
    public void activate(@PathVariable("number") String phoneNumber,
                         @RequestParam(name = "success", required = false, defaultValue = "true") boolean success) throws PhoneNumberNotFound {
        final Optional<PhoneNumber> search = phoneNumberRepository.findById(phoneNumber);
        if (search.isEmpty()) {
            throw new PhoneNumberNotFound(phoneNumber);
        }
        final PhoneNumber number = search.get();
        final ActivationAttempt attempt = ActivationAttempt.builder()
                                                           .attemptTime(LocalDateTime.now(Clock.systemUTC()))
                                                           .phoneNumber(number)
                                                           .success(success)
                                                           .build();
        // we always add to the front as expected to be sorted by latest first
        number.getActivationHistory().add(0, attempt);
        // save via the owning entity.
        phoneNumberRepository.save(number);
    }

    @ResponseStatus(NOT_FOUND)
    public static class PhoneNumberNotFound extends Exception {
        public PhoneNumberNotFound(String number) {
            super(format("%s not found", number));
        }
    }
}
