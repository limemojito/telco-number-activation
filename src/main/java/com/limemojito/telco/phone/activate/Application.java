package com.limemojito.telco.phone.activate;

import com.limemojito.telco.phone.activate.model.PhoneNumber;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Link our custom controller to the REST HATEOAS representation
     *
     * @return decorated resource.
     */
    @Bean
    public ResourceProcessor<Resource<PhoneNumber>> phoneNumberResourceProcessor() {
        //noinspection Convert2Lambda
        return new ResourceProcessor<>() {
            @Override
            public Resource<PhoneNumber> process(Resource<PhoneNumber> resource) {
                final Link activate = linkTo(ActivationController.class, resource.getContent().getNumber()).withRel("activate");
                resource.add(activate);
                return resource;
            }
        };
    }
}
