package com.limemojito.telco.phone.activate.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface PhoneNumberRepository extends PagingAndSortingRepository<PhoneNumber, String> {

    @Override
    @EntityGraph(value = "allHistory")
    Optional<PhoneNumber> findById(String id);

    Page<PhoneNumber> findByCustomerId(@Param("customerId") UUID customerId, Pageable pageable);
}
