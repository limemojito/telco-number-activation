create table phone_number
(
    number         char(9),
    customer_id     uuid not null,
    constraint pk_phone_number primary key(number),
    -- we cheat a bit here and as we know the constraint is implemented by an index we but customerId first to get some grouping.
    constraint uk_phone_number_customer unique(customer_id, number)
);