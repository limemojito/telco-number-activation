<project>
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.limemojito.oss.tutorial</groupId>
    <artifactId>telco-number-activation</artifactId>
    <version>1.0.0-SNAPSHOT</version>
    <packaging>jar</packaging>

    <parent>
        <groupId>com.limemojito.oss.standards</groupId>
        <artifactId>jar-development</artifactId>
        <version>2.0.0</version>
        <relativePath/>
    </parent>

    <properties>
        <!-- Coverage excludes for unit testing checks.  Some things are best tested at integration for wiring / configuration level -->
        <!-- We avoid the Application class as a test is pointless as we are doing a full integration spin up of the boot container -->
        <coverage.exclude.pattern.1>**Application</coverage.exclude.pattern.1>
        <!-- I don't bother with model tests as they are all annotations or trivial implementations.  Repositories are purely interfaces, and
        exercised in integration tests -->
        <coverage.exclude.pattern.2>**model*</coverage.exclude.pattern.2>

        <!-- We are not publishing and this is based on the OSS standards pom.  Disable GPG signing. -->
        <gpg.skip>true</gpg.skip>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <!--suppress MavenModelInspection -->
                <version>${version.spring.boot}</version>
                <executions>
                    <execution>
                        <id>make-jar</id>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                        <phase>package</phase>
                    </execution>
                    <execution>
                        <id>start-boot</id>
                        <goals>
                            <goal>start</goal>
                        </goals>
                        <phase>pre-integration-test</phase>
                        <configuration>
                            <!-- 120 second start with 500ms waits -->
                            <wait>500</wait>
                            <maxAttempts>240</maxAttempts>
                            <!--suppress MavenModelInspection -->
                            <skip>${skipITs}</skip>
                            <profiles>test,integration-test</profiles>
                        </configuration>
                    </execution>
                    <execution>
                        <id>stop-boot</id>
                        <goals>
                            <goal>stop</goal>
                        </goals>
                        <phase>post-integration-test</phase>
                        <configuration>
                            <!--suppress MavenModelInspection -->
                            <skip>${skipITs}</skip>
                        </configuration>
                    </execution>
                    <execution>
                        <id>build-info</id>
                        <goals>
                            <goal>build-info</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-failsafe-plugin</artifactId>
                <configuration>
                    <!-- So we can use the model objects in the integration tests as I'm lazy and they JSON well. -->
                    <additionalClasspathElements>${project.build.outputDirectory}</additionalClasspathElements>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-rest</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.8</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
        </dependency>
        <dependency>
            <groupId>org.flywaydb</groupId>
            <artifactId>flyway-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <version>2.4.1</version>
            <scope>runtime</scope>
        </dependency>


        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-webflux</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.projectreactor</groupId>
            <artifactId>reactor-spring</artifactId>
            <version>1.0.1.RELEASE</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>