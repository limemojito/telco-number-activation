# telco-number-activation
## TL;DR
* Java 11 + maven 3.6 to build
* mvn clean install
* java -Dspring.profiles.active=golden -jar target/telco-number-activation-1.0.0-SNAPSHOT.jar
* * curl http://localhost:8080/phoneNumbers/search/findByCustomerId?customerId=70939945-4655-4b3b-a0d0-b2941805e11b
* API from reuse + standards over hand build
* Focus on production code rather than array vs map vs tree vs ....
* Spring Boot + Spring Data HATEOAS implementation.
* Lombok to reduce boilerplate - use a lombok plugin in your IDE.
* Build system is similar pattern to what I have designed for other teams.
* coverage in target/site/jacoco, checkstyle, unit * integration tests, etc.

---
# API
Suggest browsing with postman as this is a self addressing HATEOAS api with pagination.  

## HATEOAS Entry Point
GET http://localhost:8080

Return 200, index json

Self describing API, mainly provided by Spring Data Rest.  For a true production deployment this would be fronted by an API Gateway, and support tokenized security.

## Phone Number Requested API
### All Phone Numbers
GET http://localhost:8080/phoneNumbers{?page,size,sort}

Return: 200 Phone Number Paginated List

Paginated list of all phone numbers in system.  Each Phone number can be drilled into to see the Activation History.

### Phone Number by customer
GET http://localhost:8080/phoneNumbers/search/findByCustomerId{?customerId,page,size,sort}

Return: 200 Phone Number Paginated List

Phone number Phone number can be drilled into to see the Activation History.

### Activate a Phone Number
POST http://localhost:8080/phoneNumbers/{number}/activate{?success}

Return: 204 NO CONTENT

This is representing an async style API as activation is normally an external system provision.  Returns 204 no content.
One could argue this should be PATCH as we are altering an attribute of the number.  You can use success to load a failed activation for testing.

## Data
The spring profile "golden" loads a golden data set.

ie:  http://localhost:8080/phoneNumbers/search/findByCustomerId?customerId=70939945-4655-4b3b-a0d0-b2941805e11b

### Phone Number Paginated List
```
{
  "_embedded" : {
    "phoneNumbers" : [ {
      "customerId" : "fd8d4891-039b-40dc-8858-ed04b9b0082d",
      "activationHistory" : [ {
        "attemptTime" : "2019-12-10T22:09:59.898694",
        "success" : true,
        "_links" : {
          "phoneNumber" : {
            "href" : "http://localhost:8080/phoneNumbers/414646441"
          }
        }
      }, {
        "attemptTime" : "2019-12-10T22:09:59.885065",
        "success" : false,
        "_links" : {
          "phoneNumber" : {
            "href" : "http://localhost:8080/phoneNumbers/414646441"
          }
        }
      }, {
        "attemptTime" : "2019-12-10T22:09:59.864688",
        "success" : false,
        "_links" : {
          "phoneNumber" : {
            "href" : "http://localhost:8080/phoneNumbers/414646441"
          }
        }
      } ],
      "active" : true,
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/phoneNumbers/414646441"
        },
        "phoneNumber" : {
          "href" : "http://localhost:8080/phoneNumbers/414646441"
        },
        "activate" : {
          "href" : "http://localhost:8080/phoneNumbers/414646441/activate"
        }
      }
    } ]
  },
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/phoneNumbers{?page,size,sort}",
      "templated" : true
    },
    "profile" : {
      "href" : "http://localhost:8080/profile/phoneNumbers"
    },
    "search" : {
      "href" : "http://localhost:8080/phoneNumbers/search"
    }
  },
  "page" : {
    "size" : 20,
    "totalElements" : 1,
    "totalPages" : 1,
    "number" : 0
  }
}
```

### Phone Number
``` 
{
     "customerId" : "fd8d4891-039b-40dc-8858-ed04b9b0082d",
     "activationHistory" : [ {
       "attemptTime" : "2019-12-10T22:09:59.898694",
       "success" : true,
       "_links" : {
         "phoneNumber" : {
           "href" : "http://localhost:8080/phoneNumbers/414646441"
         }
       }
     }, {
       "attemptTime" : "2019-12-10T22:09:59.885065",
       "success" : false,
       "_links" : {
         "phoneNumber" : {
           "href" : "http://localhost:8080/phoneNumbers/414646441"
         }
       }
     }, {
       "attemptTime" : "2019-12-10T22:09:59.864688",
       "success" : false,
       "_links" : {
         "phoneNumber" : {
           "href" : "http://localhost:8080/phoneNumbers/414646441"
         }
       }
     } ],
     "active" : true,
     "_links" : {
       "self" : {
         "href" : "http://localhost:8080/phoneNumbers/414646441"
       },
       "phoneNumber" : {
         "href" : "http://localhost:8080/phoneNumbers/414646441"
       },
       "activate" : {
         "href" : "http://localhost:8080/phoneNumbers/414646441/activate"
       }
     }
   }
```

---
# Background
We are a Telecom operator. In our database, we are starting to store phone numbers associated to customers (1 customer : N phone numbers) and we will provide interfaces to manage them.
We need to provide the below capabilities:
•	get all phone numbers
•	get all phone numbers of a single customer

Activate a phone number

## Challenge
* Provide interface specifications for the above functions / capabilities.
* Provide implementation of the formulated specifications.

You can assume the phone numbers as a static data structure that is initialised when your program runs.

## Language
Java / NodeJS

## Deliverables
Interface specifications, the source files and any test code.

The solution will be evaluated based on the following criteria:
* Correctness
* Code structure
* Data structures
* Extensibility
* Maintainability
* Test coverage
* Performance
---

# My Design Thoughts

Get and get all imply a sync API.
Activate could be sync, but from my experience an async design would be better as the activation process would most likely be a form of system integration with an unknown time frame for activates to complete.

Lets assume activate will take a phone number after it is assigned to a customer and async provision the number activation.
We will express the activation a series of state changes that are either informational until OK or ERROR.  

* Assumption - we are activating a phone number and not a SIM or other virtual circuit.

So we have an activation command, followed by a sync status style queries.  A more “productionized” design would be a callback mechanism on activate event changes.  Depending on the enterprise architecture model we could do this via messaging or web hooks.  My preference is messaging using notification for events as this scales better from an operations / implementation perspective than having to manage registered webhooks on multiple services.  Another alternative is to manage eventing via web sockets or HTTP2, however in my experience I have found messaging solutions such as SQS, Rabbit, etc to be superior to setup and maintain, as they allow the easy application of policies such as retry and dead lettering.

## Ignored for coding test:

* Security:  We will ignore API security for the coding test - happy to have a discussion on spring security, JWT, and other mechanisms.
* Auditing: We will ignore auditing as we have no security context - though spring data auditing is a hammer appropriate here with the spring data implementation choice.
* Storage:  We will keep everything very simple with a HSQLDB for the persistence tier.  I could have made the build more complicated with docker as part of the integration testing environment and postgres or similar, but I decided I was showing off enough with the spring implementation.  I will show my preferences for management of RDBMS by using flyway and managing the schema by delta script.   As an aside, given the limited query paths in the example a NoSQL database my also be considered.  My main experience is with Dynamodb.
* API Lockdown:  I will be using the standard full verb set in spring data HATEOAS to keep it simple.  You can also tailor the repositories to remove delete, etc as required.

## Data

### Customer:
Lets assume we are making a micro service for number.  So we'll take customer out of our design context, and assume we have a UUID or similar as the customer identifier.

### PhoneNumber:
number = we will use this for uniqueness
customer = link to customer that owns phone number

We use number for uniqueness - we will assume number sans call prefixes
finds:  byNumber, byCustomer, byAll

### Activation:
requestId: lets assume a UUID
requestTimestamp: timestamp of request submission
phoneNumber: link to phone number requesting activation for
status:  collection of states of activation processing.

finds:  byPhoneNumber

### ActivationStatus:
statusId: lets assume UUID
statusTimestamp: timestamp of status creation
activation: link to Activation that this status belongs to.
state: description of state activation is in.

For simplicity for this example we’ll use state as a string with either an error description or OK in it. 

## API (high level):
* /phoneNumbers VERBS
* /phoneNumbers/{number}/activate   PUT Trigger activation request (ASYNC)

Implementation in Spring Boot + Java 11, maven build system.
IDE - IntelliJ 2019.3

# Timings
* Think Time:   30 mins
* Implementation: 180 mins
* Doc Cleanup: 30 mins
